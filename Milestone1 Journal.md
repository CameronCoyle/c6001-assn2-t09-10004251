# Milestone 1 Journal #

We initially met as a group to go over key ideas, such as what app we wanted to make, what information we would retrieve, layouts etc. This meeting also included dictating roles within the group and ensuring everyone was happy with the even work load. After deciding what we would do we set goals of when things would be done, for example have wireframs written drawn by friday so we could look into further detail. We also set up communication through slack and decided to use a organisational tool called trello.
After our initial meeting I went away and did research into methods on how to tackle this project, using recources on moodle and ones I found via google.
Had another meeting with the group to finalize some ideas, such as layout for the app and how we wanted to design it. Also went over what we would need to do to accomplish our project.
I then assisted creating some of the wireframes, helping with layout and possible ideas to improve the design.
Over the weekend I was very busy with other commitments so had to have a small break from research and furthering the task.
Had one final meeting with the group to make sure we all were happy with the report and how we were tracking with the project and went over some small things.